﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial2_Excepcion
{
    class Program
    {
        static void Main(string[] args)
        {
            int edad1 = Convert.ToInt32(Console.ReadLine());
            string nombre1 = Console.ReadLine();

            if (edad1 < 0)
            {
                throw new ExcepcionEdad();
            }
            Persona persona1 = new Persona(edad1, nombre1);
            
        }
    }
}
