﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Parcial2_Excepcion
{
    class ExcepcionEdad:System.Exception
    {
        public ExcepcionEdad()
        {

        }
        public ExcepcionEdad(string mensaje):base(mensaje)
        {

        }

        public ExcepcionEdad(string mensaje, Exception innerE) : base(mensaje, innerE)
        {
            mensaje = "Edad no valida";

        }
    }
}
